FROM node:11-alpine
COPY . /src/
WORKDIR /src
RUN yarn install
CMD ["node","test-client.js"]
