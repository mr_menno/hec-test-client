The following environment variables are available:

* `HEC_ENDPOINT`: http(s)://splunk-hec-server:portnumber/services/collector/event
* `HEC_TOKEN`: configured via Splunk

* Optional `INDEX`: destination index, default is main
* Optional `COUNT`: amount of events to be set, default is 1000
* Optional `CONCURRENT`: how many concurrent web requests, default is 1
* Optional `SESSION`: to be included with the events as a reason or session identifier
* Optional `DURATION`: to time limit the test in seconds

It would be recommended to use either `DURATION` or `COUNT`, but not both.

Example:
```
export HEC_ENDPOINT=<...>
export HEC_TOKEN=<...>
export CONCURRENT=10
export SESSION="test session"
export DURATION=180
export INDEX=testindex
node ./test-client.js
```

Alternatively, the dockerfile can be leveraged:
```
docker build -t hec-test .
docker run -e HEC_ENDPOINT=<...> -e HEC_TOKEN=<...> hec-test
```