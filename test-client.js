let HEC_ENDPOINT = process.env.HEC_ENDPOINT;
let HEC_TOKEN = process.env.HEC_TOKEN;
let request = require('request');
let RELEASE = {};
try {
  RELEASE = JSON.parse(require('fs').readFileSync('release.json', 'utf8'));
} catch(e) {
  RELEASE.commit='develop';
  RELEASE.date=new Date().toString();
}

console.log(`Starting hec-test-client for commit [${RELEASE.commit}] built on ${RELEASE.date}`);

let CLIENT_ID = require('uuid').v4();
let COUNT = parseInt(process.env.COUNT)||1000;
let CONCURRENT = parseInt(process.env.CONCURRENT)||1;
let SESSION = process.env.SESSION || "not_set";
let DURATION = parseInt(process.env.DURATION)||0;
let INDEX = process.env.INDEX||"main";
let START = new Date().getTime();
let responses = {};
let sent=0; let received=0;
let progress = 0;

let send_results = function() {
    let results = {
        clientid: CLIENT_ID,
        sent: sent,
        received: received,
        summary: Object.keys(responses).map(k => {
            return {result:k,count:responses[k]}
        }),
        event_target: COUNT,
        duration: (new Date().getTime() - START)/1000,
        concurrency: CONCURRENT,
        session: SESSION,
        release: RELEASE
    }
    request.post(HEC_ENDPOINT,{
        headers: {
            Authorization: 'Splunk ' + HEC_TOKEN
        },
        json: {
            index: INDEX,
            sourcetype: 'hec-test-client:summary',
            event: results
        }
    },(err,res,body) => {
        console.log('submitted results');
        console.log(results);
        process.exit();
    });
}

let send_test = function() {
    if(DURATION==0 && sent>=COUNT) {
        return;
    }
    sent++;
    request.post(HEC_ENDPOINT,{
        headers: {
            Authorization: 'Splunk ' + HEC_TOKEN
        },
        json: {
            index: INDEX,
            sourcetype: 'hec-test-client:event',
            event: {
                clientid: CLIENT_ID,
                data: 'this is some event data',
                session: SESSION,
                release: RELEASE,
            }
        }
    },(err,res,body) => {
        received++;
        progress++;
        let now = new Date().getTime();
        if((DURATION>0 && now<START+(DURATION*1000)) || (DURATION==0 && progress<COUNT)) setImmediate(() => send_test());
        if(err) {
            if(!responses[err.toString()]) responses[err.toString()]=0;
            responses[err.toString()]++;
            // send_results();
            if(progress==COUNT) {
                console.log('send_results::1');
                send_results();
            }
            return;
        }
        let result=res.statusCode+" "+res.statusMessage+" BODY:"+JSON.stringify(body||{});
        if(!responses[result]) responses[result]=0;
        responses[result]++;
        if(progress==COUNT && DURATION===0) {
            console.log('send_results::2');
            send_results();
        }
    });
}

for(let c=0;c<CONCURRENT;c++) {
    setTimeout(send_test,c*10);
}

process.on('SIGINT',() => {
    // console.log(responses);
    send_results();
})
process.on('SIGTERM',() => {
    // console.log(responses);
    send_results();
})
process.on('exit',() => {
    // console.log(responses);
})

if(DURATION>0) {
    setTimeout(() => {
        send_results();
    },DURATION*1000+5000)
}
